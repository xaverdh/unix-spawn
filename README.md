# unix-spawn

## What it is

This is a haskell library which allows to spawning a child process
in a defined state.

That is nontrivial in (ghc) haskell for several reasons:
  * Haskell programs often cannot keep track of all open file descriptors due to interference by either the ghc runtime or common libraries.
  * The unix / posix library exposes only the fork syscall
  * The unix / posix library does not currently expose CLOEXEC

## How it works

The current implementation proceeds as follows:
  * Use the fork syscall to create a new child process
  * Try to close _all_ file descriptors except for those still needed. This means potentially attempting to close a HUGE number of fds ( as a side note ghc actually does the same in System.Process )
  * Set up the supplementary Groups, GID and UID of the new process





