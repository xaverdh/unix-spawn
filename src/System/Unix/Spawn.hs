{-# language GADTs #-}
module System.Unix.Spawn
  ( spawn
  , inheritPolicy
  , cleanPolicy
  , SpawnPolicy(..)
  , FdPolicy(..)
  , FdFailureMode(..)
  , NoMaxNumFdsException(..) )
where

import System.Unix.Spawn.Internal.FileDescriptors

import System.Posix.User
import System.Posix.Process (forkProcess)
import System.Posix.Types (ProcessID,UserID,GroupID,Fd)

import Control.Monad.Extra


-- | Policy for setting up the file descriptors.
data FdPolicy where
  InheritFds :: (Functor t,Foldable t) => t Fd -> FdPolicy
  -- Inherit /only/ the given file descriptors, any other
  --   open file descriptor will be closed in the child.
  CloseFds :: Foldable t => t Fd -> FdPolicy
  -- Close the given file descriptors, any other
  --   open file descriptors will be inherited by the child.

-- | Set up the file descriptors accrding to given /FdPolicy/.
--   Expects the maximum number of file descriptor as argument.
setupFds :: Int -- ^ Maximum number of file descriptors
  -> FdPolicy
  -> IO ()
setupFds maxFds policy = case policy of
  InheritFds fds -> keepFds fds maxFds
  CloseFds fds -> closeFds fds

-- | Set up the childs user id.
setupUID :: Maybe UserID -> IO ()
setupUID mbUID = whenJust mbUID setUserID

-- | Set up the childs group id.
setupGID :: Maybe GroupID -> IO ()
setupGID mbGID = whenJust mbGID setGroupID

-- | Set up the childs supplementary group ids.
setupGroups :: Maybe [GroupID] -> IO ()
setupGroups mbGroups = whenJust mbGroups setGroups

-- | Controls the state inherited by the child process in /spawn/.
data SpawnPolicy = 
  SpawnPolicy {
  -- | What file descriptors should be inherited ?
    fdPolicy :: FdPolicy
  -- | What happens if problems occur in the file descriptor setup ?
  , fdFailureMode :: FdFailureMode
  -- | Optionally change the uid of the child to given value.
  , uidPolicy :: Maybe UserID
  -- | Optionally change the gid of the child to given value.
  , gidPolicy :: Maybe GroupID
  -- | Optionally change the supplementary group ids of the
  --   child to given value.
  , groupsPolicy :: Maybe [GroupID]
  }


-- | Start with a clean state (as far as possible).
--   It will close all file descriptors and
--   clear the supplementary group list.
--   Uses the strictest possible failure mode for /FdFailureMode/.
--   If you have root privileges, you probably want to
--   use this as
-- @   
--    cleanPolicy {
--      uidPolicy = my_unprivileged_uid
--    , gidPolicy = my_unprivileged_gid
--    }
-- @
--
cleanPolicy :: SpawnPolicy
cleanPolicy = SpawnPolicy
  (InheritFds [])
  FailOnUnknownRes
  Nothing
  Nothing
  (Just [])

-- | Inherit everything from the parent.
inheritPolicy :: SpawnPolicy
inheritPolicy = SpawnPolicy
  (CloseFds [])
  FailNever
  Nothing
  Nothing
  Nothing


-- | Spawn a child process with precise control over the
--   inherited state as specified by the /SpawnPolicy/ argument.
--   If something goes wrong with the file descriptor setup,
--   it will throw /NoMaxNumFdsException/.
spawn :: SpawnPolicy -- ^ The state inheritance policy
  -> IO () -- ^ The IO action to execute in the child process
  -> IO ProcessID 
spawn policy action =
  getMaxNumFds (fdFailureMode policy)
  >>= \maxFds -> forkProcess $ do
      setupFds maxFds $ fdPolicy policy
      setupGroups $ groupsPolicy policy 
      setupGID $ gidPolicy policy
      setupUID $ uidPolicy policy
      action





