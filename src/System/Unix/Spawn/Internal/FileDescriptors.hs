{-# language GADTs #-}
module System.Unix.Spawn.Internal.FileDescriptors
  ( closeFds
  , getMaxNumFds
  , keepFds
  , FdFailureMode(..)
  , NoMaxNumFdsException(..) )
where

import Control.Monad.IO.Class
import Control.Exception

import System.Posix.Resource
import System.Posix.IO
import System.Posix.Types
import Data.Typeable
import qualified Data.IntSet as S
import qualified Data.Foldable as F

-- | Close Fd, ignoring any errors.
fdWeakClose :: Fd -> IO ()
fdWeakClose pfd = catch (closeFd pfd)
  (\e -> return $ const () (e::IOException) )

-- | What should happen if we have trouble determining
--   the maximum number of file descriptors ?
data FdFailureMode =
  FailNever
  -- ^ I don't care, just make it work
  | UseThis Int
  -- ^ I know the maximum, use this
  | FailOnInfiniteRes
  -- ^ Fail if the maximum number is infinity.
  --   This is sensible because we cannot close an infinite
  --   number of file descriptors in finite time.
  --   If the maximum number of file descriptors is unknown
  --   we will still try to guess.
  | FailOnUnknownRes
  -- ^ Like /FailOnInfiniteRes/ but also fail if the maximum
  --   number of file descriptors is unknown (instead of guessing).
  --   This is the strictest option.

-- | Exception thrown by getMaxNumFds if something goes wrong
data NoMaxNumFdsException =
  MaxNumFdsWasInfinityException
  | MaxNumFdsWasUnknownException
  deriving (Typeable,Show)

instance Exception NoMaxNumFdsException where
  displayException e = case e of
    MaxNumFdsWasInfinityException ->
      "The maximum number of file descriptors was infinite."
    MaxNumFdsWasUnknownException ->
      "The maximum number of file descriptors was unknown."

-- | Get the maximum number of allowed file descriptors
--   or throws an /NoMaxNumFdsException/ if something went wrong.
getMaxNumFds :: FdFailureMode -> IO Int
getMaxNumFds failMode = do
  lim <- softLimit <$> getResourceLimit ResourceOpenFiles
  case lim of
    ResourceLimitUnknown -> case failMode of
      UseThis i -> pure i
      FailOnUnknownRes ->
        throw MaxNumFdsWasUnknownException
      FailNever -> pure (2^8)
      FailOnInfiniteRes -> pure (2^8)
        -- Conservative guess (ghc guesses the same
        -- when closing all handles in createProcess).
    ResourceLimitInfinity -> case failMode of
      UseThis i -> pure i
      FailOnUnknownRes ->
        throw MaxNumFdsWasInfinityException
      FailNever -> pure (2^12) -- Random high number
      FailOnInfiniteRes ->
        throw MaxNumFdsWasInfinityException
    ResourceLimit i -> pure $ fromInteger i

-- | Close all file descriptors _except_ for those given.
--   Expects the maximum number of file descriptor as argument.
keepFds :: (Functor t,Foldable t)
  => t Fd -- ^ File descriptors to close
  -> Int -- ^ Maximum number of file descriptors
  -> IO ()
keepFds keepMe maxFds = do
  mapM_ fdWeakClose (toEnum <$> S.toList closeThese)
  where
    closeThese = allFds `S.difference` keepThese
    keepThese = fromFoldable (fromEnum <$> keepMe)
    allFds = S.fromList [ 0 .. maxFds ]
    
    fromFoldable = F.foldl' ins S.empty
    ins = flip S.insert

-- | Close the given file descriptors.
closeFds :: Foldable t
  => t Fd -- ^ File descriptors to close
  -> IO ()
closeFds = mapM_ fdWeakClose


